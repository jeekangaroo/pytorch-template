import argparse
import json
import os
import sys
from collections import OrderedDict
from shutil import copy, copytree, ignore_patterns

import numpy as np
import onnxruntime
import torch.onnx
import yaml

sys.path.append('./')
import model


def to_numpy(tensor):
    return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()


def main(cfg):
    path = os.path.join("deploy", os.path.splitext(cfg.name)[0])
    if os.path.exists(path):
        print("Name already used, please choose a different name!")
        return -1
    else:
        os.mkdir(path)

    info_dict = {"name": cfg.name,
                 "input_size": cfg.input,
                 "model": {"arch": cfg.model},
                 "checkpoint": os.path.basename(cfg.ckpt) if cfg.ckpt is not None else "BASE"}

    # Parse Arguments to create Model instance
    model_class = getattr(model, cfg.model)
    print("Created Pytorch Model!")

    # Load state dict from checkpoint
    full_config = None
    if cfg.ckpt is not None:
        ckpt = torch.load(cfg.ckpt, map_location=torch.device('cpu'))
        full_config = ckpt['config']
        model_config = dict(ckpt['config']['arch']['args'])
        info_dict["model"].update({"args": model_config})
        if len(model_config.keys()) > 0:
            print("Model Configuration:\n{}\n".format("\n".join([str(k) + " : " + str(v) for k, v in model_config.items()])))
        else:
            print("Model Configuration:\n default")

        model_instance = model_class(**model_config)
        state_dict = ckpt['state_dict']

        # If state_dict was saved from DataParallel Model, change state_dict
        if list(state_dict.keys())[0][:6] == "module":
            new_state_dict = OrderedDict()
            for k, v in state_dict.items():
                name = k[7:]
                new_state_dict[name] = v
            state_dict = new_state_dict

        # Load state dict
        model_instance.load_state_dict(state_dict)
    else:
        model_instance = model_class()

    # Model Eval mode
    model_instance.eval()
    print("Successfully loaded state_dict!")

    # Create Dummy Input and Output for Testing
    x = torch.randn(*cfg.input, requires_grad=True)
    torch_out = model_instance(x)

    # Exporting the Model
    torch.onnx.export(model_instance,  # model to export
                      x,  # Dummy Input
                      os.path.join(path, cfg.name),  # Where to Save Model
                      export_params=True,  # Store trained parameter weights
                      do_constant_folding=True,  # Optimization
                      opset_version=11,
                      input_names=['input'],
                      output_names=['output'],
                      dynamic_axes={'input': {2: 'height', 3: 'width'}}
                      )

    print("Exported model to ONNX file")

    print("Testing Output Validity...")
    # Test whether outputs are same within reasonable distance
    ort_session = onnxruntime.InferenceSession(os.path.join(path, cfg.name), providers=["CPUExecutionProvider"])

    # Compute ONNX Runtime output prediction
    ort_inputs = {ort_session.get_inputs()[0].name: to_numpy(x)}
    ort_outs = ort_session.run(None, ort_inputs)

    print(f"Exported model saved under deploy/{os.path.splitext(cfg.name)[0]} as {cfg.name}")

    # Organise the saved files in one folder
    if cfg.ckpt is not None:
        copy(cfg.ckpt, os.path.join(path, os.path.basename(cfg.ckpt)))
        copytree("./model", os.path.join(path, "model"), ignore=ignore_patterns("loss.py", "__init__.py", "metric.py"))
        with open(os.path.join(path, "info.json"), 'w') as f:
            json.dump(info_dict, f, indent=4)
        with open(os.path.join(path, 'config.yaml'), 'w') as file:
            yaml.dump(full_config, file, indent=4, default_flow_style=False)

    # compare ONNX Runtime and PyTorch results
    np.testing.assert_allclose(to_numpy(torch_out), ort_outs[0], rtol=1e-03, atol=1e-05)
    print("Exported model has been tested with ONNXRuntime, and the result looks good!")


if __name__ == "__main__":
    # Command line argument parser
    parser = argparse.ArgumentParser(description="ONNX Converter")
    parser.add_argument('-m', '--model', help='Model Class Name', required=True)
    parser.add_argument('-c', '--ckpt', help='Model Checkpoint', default=None)
    parser.add_argument('-i', '--input', nargs="+", help='Input Size', type=int, required=True)
    parser.add_argument('-n', '--name', help="ONNX Model Name", required=True)

    args = parser.parse_args()
    main(args)
