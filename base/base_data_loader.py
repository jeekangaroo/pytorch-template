import numpy as np
from torch.utils.data.dataloader import DataLoader, default_collate
from torch.utils.data.sampler import SubsetRandomSampler


class BaseDataLoader(DataLoader):
    """
    Base class for all data loaders
    """
    def __init__(self, dataset, batch_size, shuffle, validation_split, num_workers,
                 distributed=False, sampler=None, collate_fn=default_collate, prefetch_factor=2):

        # If train and validation set are given separately (e.g. using Subset)
        if isinstance(dataset, tuple):
            self.dataset, self.valid_dataset = dataset
            self.n_samples, self.n_val_samples = len(self.dataset), len(self.valid_dataset)
            self.validation_split = 0.0
        else:
            # Case when validation set will be created from the whole dataset
            self.dataset = dataset
            self.n_samples = len(self.dataset)
            self.validation_split = validation_split

        # Flag that governs whether to shuffle the dataset or not
        self.shuffle = shuffle

        # If sampler is not given, use default split_sampler to separate training and validation set
        if sampler is None:
            self.sampler, self.valid_sampler = self._split_sampler(self.validation_split)
        else:
            # If a sampler is given, use that for the training set dataloader / validation set sampler defaults.
            self.sampler = sampler
            _, self.valid_sampler = self._split_sampler(self.validation_split)

        # Main parameters for the PyTorch Dataloader class
        self.init_kwargs = {
            'dataset': self.dataset,
            'batch_size': batch_size,
            'shuffle': self.shuffle,
            'collate_fn': collate_fn,
            'num_workers': num_workers,
            'prefetch_factor': prefetch_factor,
        }

        # Turn off pin_memory when training in a distributed manner
        if not distributed:
            self.init_kwargs.update({'pin_memory': True})

        super().__init__(sampler=self.sampler, **self.init_kwargs)

    # Split sampler
    def _split_sampler(self, split):
        # If validation set is given separately, number of samples is given by the length of that separate set
        if hasattr(self, 'n_val_samples'):
            n_smp = self.n_val_samples
        else:
            n_smp = self.n_samples

        # If not splitting, then no sampler needed
        if split == 0.0:
            return None, None

        idx_full = np.arange(n_smp)

        np.random.seed(0)
        np.random.shuffle(idx_full)

        # Split can be given as an exact number
        if isinstance(split, int):
            assert split > 0
            assert split < n_smp, "validation set size is configured to be larger than entire dataset."
            len_valid = split
        else:
            # Or as a ratio
            len_valid = int(n_smp * split)

        valid_idx = idx_full[0:len_valid]
        train_idx = np.delete(idx_full, np.arange(0, len_valid))

        train_sampler = SubsetRandomSampler(train_idx)
        valid_sampler = SubsetRandomSampler(valid_idx)

        # Turn off shuffle option which is mutually exclusive with sampler
        self.shuffle = False

        # Number of samples now change as we have split the entire dataset into a training set and a validation set
        if not hasattr(self, 'n_val_samples'):
            self.n_samples = len(train_idx)

        return train_sampler, valid_sampler

    def split_validation(self):
        # This function returns the Validation Dataset Dataloader
        if self.valid_sampler is None and not hasattr(self, 'valid_dataset'):
            return None
        else:
            if hasattr(self, 'valid_dataset'):
                # If validation dataset is provided separately, use that as the dataset instead of using the sampler.
                self.init_kwargs['dataset'] = self.valid_dataset
            return DataLoader(sampler=self.valid_sampler, **self.init_kwargs)
