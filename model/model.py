import torch.nn as nn
import torch.nn.functional as F
from base import BaseModel

from model.archs.efficientnetv2 import gen_efficientnetv2

from model import conv_bn_relu


def EfficientNetV2(in_channels=3, channel_multiplier=1.0, depth_multiplier=1.0, num_features=1280, num_classes=10):
    model = gen_efficientnetv2(
        in_chans=in_channels,
        channel_multiplier=channel_multiplier,
        depth_multiplier=depth_multiplier,
        num_features=num_features,
        num_classes=num_classes
        )
    return model


class MnistModel(BaseModel):
    def __init__(self, num_classes=10):
        super().__init__()
        self.conv1 = conv_bn_relu(1, 10, 5)
        self.conv2 = conv_bn_relu(10, 20, 5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(20*5*5, 50)
        self.fc2 = nn.Linear(50, num_classes)

    def forward(self, x):
        x = F.max_pool2d(self.conv1(x), 2)
        x = F.max_pool2d(self.conv2_drop(self.conv2(x)), 2)
        x = x.view(-1, 20*5*5)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return x
