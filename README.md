# PyTorch Template Project
### Jeehoon Kang
LAST UPDATED: 11th Jan. 2021

PyTorch deep learning project made easy.

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

* [PyTorch Template Project](#pytorch-template-project)
	* [Requirements](#requirements)
	* [Features](#features)
	* [Folder Structure](#folder-structure)
	* [Usage](#usage)
		* [Config file format](#config-file-format)
		* [Using config files](#using-config-files)
		* [Resuming from checkpoints](#resuming-from-checkpoints)
    * [Using Multiple GPU](#using-multiple-gpu)
	* [Customization](#customization)
		* [Custom CLI options](#custom-cli-options)
		* [Data Loader](#data-loader)
		* [Trainer](#trainer)
		* [Model](#model)
		* [Loss and metrics](#loss-and-metrics)
			* [Multiple metrics](#multiple-metrics)
		* [Additional logging](#additional-logging)
		* [Validation data](#validation-data)
		* [Checkpoints](#checkpoints)
	* [Deployment](#deployment)
	    * [ONNX](#onnx)
	    * [TorchScript](#torchscript)
	* [Acknowledgements](#acknowledgments)

<!-- /code_chunk_output -->

## Requirements
* Python >= 3.6
* PyTorch >= 1.7.1
* Python packages listed in `requirements.txt`

## Features
* Clear folder structure which is suitable for many deep learning projects.
* `.json` config file support for convenient parameter tuning.
* Customizable command line options for more convenient parameter tuning.
* Checkpoint saving and resuming.
* Abstract base classes for faster development:
  * `BaseTrainer` handles checkpoint saving/resuming, training process logging, and more.
  * `BaseDataLoader` handles batch generation, data shuffling, and validation data splitting.
  * `BaseModel` provides basic model summary.

## Folder Structure
  ```
  pytorch-template/
  │
  ├── train.py - main script to start training
  ├── test.py - evaluation of trained model
  │
  ├── convert_hdf.py - script to control/test the hdf5 conversion of datasets
  │
  ├── config.json - holds configuration for training
  ├── parse_config.py - class to handle config file and cli options
  │
  ├── new_project.py - initialize new project with template files
  │
  ├── base/ - abstract base classes
  │   ├── base_data_loader.py
  │   ├── base_model.py
  │   └── base_trainer.py
  │
  ├── data_loader/ - anything about data loading goes here
  │   └── data_loaders.py
  │
  ├── deploy/ - Convert trained models into onnx models or torchscript models for deployment
  │   ├── onnx_convert.py
  │   └── torchscript_convert.py
  │
  ├── data/ - default directory for storing input data
  │
  ├── model/ - models, losses, and metrics
  │   ├── archs - folder that holds various architectures used in modelling
  │   ├── model.py
  │   ├── metric.py
  │   └── loss.py
  │
  ├── saved/
  │   ├── models/ - trained models are saved here
  │   └── log/ - default logdir for tensorboard and logging output
  │
  ├── trainer/ - trainers
  │   └── trainer.py
  │
  ├── logger/ - module for tensorboard visualization and logging
  │   ├── visualization.py
  │   ├── logger.py
  │   └── logger_config.json
  │  
  └── utils/ - small utility functions, custom optimizers, etc.
      ├── util.py
      └── ...
  ```

## Usage
The code in this repo is an MNIST example of the template.
Try `python train.py -c config.json` to run code.

### Config file format
Config files are in `.json` format:
```javascript
{
  "name": "Template",                  // training session name
  "n_gpu": 1,                          // number of GPUs to use for training.
  "distributed": true,                 // Distributed Data Parallel, n_gpu is the world_size!
  "track_experiment": true,            // WandB Track Experiment

  "arch": {
    "type": "MnistModel",                // name of model architecture to train
    "args": {}
    },

  "dataloader": {
    "type": "MnistDataLoader",         // selecting data loader
    "args":{
      "h5": false,                     // flag to indicate whether to use standard dataset vs h5 datset
      "data_dir": "data/MNIST",        // dataset directory path / path to .h5 files
      "batch_size": 64,                // batch size
      "shuffle": true,                 // shuffle training data before splitting
      "validation_split": 0.1          // size of validation dataset. float(portion) or int(number of samples)
      "num_workers": 2,                // number of cpu processes to be used for data loading
      "prefetech_factor": 4            // prefetch factor for the Dataloader      
    }
  },

  "optimizer": {
    "type": "Ranger",                  // Optimizer name
    "module": "utils.ranger",          // path to module that defines custom optimizer (default: torch.optim)
    "args":{
      "lr": 0.001,                     // learning rate, arguments for optimizer
    }
  },

  "loss": "nll_loss",                  // loss

  "metrics": [
    "accuracy", "top_k_acc"            // list of metrics to evaluate
  ],           
              
  "lr_scheduler": {
    "type": "CosineAnnealingLR",       // learning rate scheduler
    "args":{
      "T_max": 30,          
    }
  },

  "trainer": {
    "amp": false,                      // Automatic Mixed Precision Training

    "grad_clip": true,                 // Gradient Clipping
    "grad_clip_value": 5,              // Gradient Clipping Value if Clipping = True

    "epochs": 30,                      // number of training epochs
    "warm_up": 2,                      // learning rate warm up (num epochs)

    "save_dir": "saved/",              // checkpoints are saved in save_dir/models/name
    "save_period": 1,                  // save checkpoints every save_period epochs
    "verbosity": 2,                    // 0: quiet, 1: per epoch, 2: full

    "monitor": "min val_loss"          // mode and metric for model performance monitoring. set 'off' to disable.
    "early_stop": 10	               // number of epochs to wait before early stop. set 0 to disable.
  }
}
```

Add additional configurations if you need to.

### Using config files
Modify the configurations in `.json` config files, then run:

  ```
  python train.py --config config.json
  ```

### Resuming from checkpoints
You can resume from a previously saved checkpoint by:

  ```
  python train.py --resume path/to/checkpoint
  ```

### Using Multiple GPU
You can enable multi-GPU training by setting `n_gpu` argument of the config file to larger number.
If configured to use smaller number of gpu than available, first n devices will be used by default.
Specify indices of available GPUs by cuda environmental variable.
  ```
  python train.py --device 2,3 -c config.json
  ```
  This is equivalent to
  ```
  CUDA_VISIBLE_DEVICES=2,3 python train.py -c config.py
  ```

## Customization

### Project initialization
Use the `new_project.py` script to make your new project directory with template files.
`python new_project.py <root_path>/NewProject` then a new project folder named 'NewProject' will be made under the root path.
This script will filter out unnecessary files like cache, git files or readme file. 

### Custom CLI options

Changing values of config file is a clean, safe and easy way of tuning hyper-parameters. However, sometimes
it is better to have command line options if some values need to be changed too often or quickly.

This template uses the configurations stored in the json file by default, but by registering custom options as follows
you can change some of them using CLI flags.

```python
import collections

# simple class-like object having 3 attributes, `flags`, `type`, `target`.
CustomArgs = collections.namedtuple('CustomArgs', 'flags type target')
options = [
  CustomArgs(['--lr', '--learning_rate'], type=float, target=('optimizer', 'args', 'lr')),
  CustomArgs(['--bs', '--batch_size'], type=int, target=('dataloader', 'args', 'batch_size'))
  # options added here can be modified by command line flags.
]
```
`target` argument should be sequence of keys, which are used to access that option in the config dict. In this example, `target` 
for the learning rate option is `('optimizer', 'args', 'lr')` because `config['optimizer']['args']['lr']` points to the learning rate.
`python train.py -c config.json --bs 256` runs training with options given in `config.json` except for the `batch size`
which is increased to 256 by command line options.


### Data Loader
* **Writing your own data loader**

1. **Inherit ```BaseDataLoader```**

    `BaseDataLoader` is a subclass of `torch.utils.data.DataLoader`, you can use either of them.

    `BaseDataLoader` handles:
    * Generating next batch
    * Data shuffling
    * Generating validation data loader by calling
    `BaseDataLoader.split_validation()`

* **DataLoader Usage**

  `BaseDataLoader` is an iterator, to iterate through batches:
  ```python
  for batch_idx, (x_batch, y_batch) in enumerate(data_loader):
      pass
  ```
* **Example**

  Please refer to `data_loader/data_loaders.py` for an MNIST data loading example.

### Trainer
* **Writing your own trainer**

1. **Inherit ```BaseTrainer```**

    `BaseTrainer` handles:
    * Training process logging
    * Checkpoint saving
    * Checkpoint resuming
    * Reconfigurable performance monitoring for saving current best model, and early stop training.
      * If config `monitor` is set to `max val_accuracy`, which means then the trainer will save a checkpoint `model_best.pth` when `validation accuracy` of epoch replaces current `maximum`.
      * If config `early_stop` is set, training will be automatically terminated when model performance does not improve for given number of epochs. This feature can be turned off by passing 0 to the `early_stop` option, or just deleting the line of config.

2. **Implementing abstract methods**

    You need to implement `_train_epoch()` for your training process, if you need validation then you can implement `_valid_epoch()` as in `trainer/trainer.py`

* **Example**

  Please refer to `trainer/trainer.py` for MNIST training.

* **Iteration-based training**

  `Trainer.__init__` takes an optional argument, `len_epoch` which controls number of batches(steps) in each epoch.

### Model
* **Writing your own model**

1. **Inherit `BaseModel`**

    `BaseModel` handles:
    * Inherited from `torch.nn.Module`
    * `__str__`: Modify native `print` function to prints the number of trainable parameters.

2. **Implementing abstract methods**

    Implement the forward pass method `forward()`

* **Example**

  Please refer to `model/model.py` for a LeNet example.

### Loss
Custom loss functions can be implemented in 'model/loss.py'. Use them by changing the name given in "loss" in config file, to corresponding name.

#### Metrics
Metric functions are located in 'model/metric.py'.

You can monitor multiple metrics by providing a list in the configuration file, e.g.:
  ```json
  "metrics": ["accuracy", "top_k_acc"],
  ```

### Additional logging
If you have additional information to be logged, in `_train_epoch()` of your trainer class, merge them with `log` as shown below before returning:

  ```python
additional_log = {"gradient_norm": g, "sensitivity": s}
log.update(additional_log)
return log
  ```
- You can also update what logs to send to WandB for tracking.
- The `watchlist` option in the config JSON file is an example of logging image files for WandB, check out the `trainer.py` file.

### Testing
You can test trained model by running `test.py` passing path to the trained checkpoint by `--resume` argument.

### Validation data
To split validation data from a data loader, call `BaseDataLoader.split_validation()`, then it will return a data loader for validation of size specified in your config file.
The `validation_split` can be a ratio of validation set per total data(0.0 <= float < 1.0), or the number of samples (0 <= int < `n_total_samples`).

**Note**: the `split_validation()` method will modify the original data loader
**Note**: `split_validation()` will return `None` if `"validation_split"` is set to `0`

### Checkpoints
You can specify the name of the training session in config files:
  ```json
  "name": "MNIST_LeNet",
  ```

The checkpoints will be saved in `save_dir/name/timestamp/checkpoint_epoch_n`, with timestamp in mmdd_HHMMSS format.

(or in `save_dir/name/timestamp/wandb_run_id` if `track_experiment` is `True` )

A copy of config file will be saved in the same folder.

**Note**: checkpoints contain:
  ```python
  {
    'arch': arch,
    'epoch': epoch,
    'state_dict': self.model.state_dict(),
    'optimizer': self.optimizer.state_dict(),
    'monitor_best': self.mnt_best,
    'config': self.config (dict containing config.json)
  }
  ```

## Deployment
Convert your models into a deployable format in the `deploy` folder
### ONNX
1. Place your model files inside the `model` folder
2. Use the command line tool to convert the PyTorch model into a ONNX model
3. The converted model will be saved under the `/deploy` folder
```
python onnx_convert.py -m <Model Class Name>
                    -i <Model Input Size>
                    -c <Path to trained .pth file>
                    -n <Output ONNX File Name (write full name with extension .onnx)>
```
- The input size should be separated by spaces

Under the `/deploy` folder you should be able to see the converted model in the following structure

```
  deploy/
  │
  ├── <converted_name>
      │
      ├── <converted_name>.onnx           // Converted ONNX file
      ├── checkpoint-epochN.pth           // Checkpoint that ONNX file was created from
      ├── info.json                       // Information on model
      ├── config.json                     // config.json at time of training
      └── model                           // Folder holding a copy of the 'model' folder
```

### TorchScript
1. Place your model files inside the `model` folder
2. Use the command line tool to convert the PyTorch model into a TorchScript model
```
python torchscript_convert.py -m <Model Class Name>
                    -i <Model Input Size>
                    -c <Path to trained .pth file>
                    -q [Optional Flag - to Quantize model]
                    -o [Optional Flag - to Optimize model for Mobile]
                    -n <Output File Name (write full name with extension .pt)>
```
- The input size should be separated by spaces

- Example:
```
python torchscript_convert.py -m MyClassfication
                    -i 1 3 256 256
                    -c trained_files/checkpoint_epoch120.pth
                    -q 
                    -n cls_quant.pt
```



## Acknowledgements
This project is based on the project [pytorch--template](https://github.com/victoresque/pytorch-template)
