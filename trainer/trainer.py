import numpy as np
import torch
from torch.cuda.amp import GradScaler, autocast

from base import BaseTrainer
from utils import MetricTracker, get_lr


class Trainer(BaseTrainer):
    def __init__(self, rank, config, experiment, tracker):
        super().__init__(rank, config, experiment)
        self.config = config
        self.tracker = tracker

        # Dataloader
        self.dataloader = experiment.dataloader
        self.valid_dataloader = experiment.validation_dataloader
        self.do_validation = self.valid_dataloader is not None

        # Epoch related settings
        self.len_epoch = len(self.dataloader)
        self.log_step = int(np.sqrt(self.dataloader.batch_size))
        self.init_lr = config['optimizer']['args']['lr']
        self.warm_up = config['trainer']['warm_up'] if 'warm_up' in config['trainer'] else 0

        # Gradient scaling for Automatic Mixed Precision
        if config['trainer']['amp']:
            self.scaler = GradScaler()

        # Metric Trackers
        if self.rank == 0:
            self.train_metrics = MetricTracker('loss', *[m.__name__ for m in self.metric_ftns])
            self.valid_metrics = MetricTracker('loss', *[m.__name__ for m in self.metric_ftns])

    def _train_epoch(self, epoch):
        """
        :param epoch: Integer, current training epoch.
        :return: A log that contains average loss and metric in this epoch.
        """
        self.model.train()  # Set model to train mode

        if self.rank == 0:
            self.train_metrics.reset()

        # Set epoch for the DistributedSampler -> shuffle across epochs
        if self.config['distributed']:
            self.dataloader.sampler.set_epoch(epoch)

        # -----  MAIN TRAINING LOOP  -----
        lr = 0.0
        for batch_idx, (data, target, file_indices) in enumerate(self.dataloader):
            if data is None:
                print("Batch is None, skipping...")
                continue

            # Send data to device (cpu, cuda:rank, mps)
            data, target = data.to(self.device, non_blocking=True), target.to(self.device, non_blocking=True)

            # Linear Learning Rate Warm-up
            full_batch_idx = ((epoch - 1) * len(self.dataloader) + batch_idx)
            if epoch - 1 < self.warm_up:
                for params in self.optimizer.param_groups:
                    params['lr'] = self.init_lr / (self.warm_up * len(self.dataloader)) * full_batch_idx
            lr = get_lr(self.optimizer)

            # -------- TRAINING LOOP --------
            # self.optimizer.zero_grad()
            for param in self.model.parameters():
                param.grad = None

            if self.config['trainer']['amp']:
                # Auto cast to mixed precision
                with autocast():
                    output = self.model(data)
                    loss = self.criterion(output, target)
                self.scaler.scale(loss).backward()

                # Unscale for gradient clipping
                if self.config['trainer']['grad_clip']:
                    self.scaler.unscale_(self.optimizer)
                    torch.nn.utils.clip_grad_norm_(self.model.parameters(), self.config['trainer']['grad_clip_value'])

                # Update Parameter!
                self.scaler.step(self.optimizer)
                self.scaler.update()
            else:
                # Full Precision Training
                output = self.model(data)
                loss = self.criterion(output, target)
                loss.backward()
                if self.config['trainer']['grad_clip']:
                    torch.nn.utils.clip_grad_norm_(self.model.parameters(), self.config['trainer']['grad_clip_value'])
                self.optimizer.step()
            # -------------------------------

            if self.rank == 0:
                # Update Training Metrics
                self.train_metrics.update('loss', loss.item())
                for met in self.metric_ftns:
                    self.train_metrics.update(met.__name__, met(output, target))

                # Log training progress
                if batch_idx % self.log_step == 0:
                    printer = self.logger.debug if not self.config['distributed'] else print
                    printer(
                        'Train Epoch: {} {} Loss: {:.6f},    lr: {:.10f}'.format(
                            epoch,
                            self._progress(batch_idx),
                            loss.item(),
                            get_lr(self.optimizer)
                        )
                    )

            if batch_idx == self.len_epoch:
                break
        # ------ END OF TRAINING LOOP ------

        # Update Learning rate scheduler
        if self.lr_scheduler is not None:
            if type(self.lr_scheduler) == torch.optim.lr_scheduler.ReduceLROnPlateau:
                self.lr_scheduler.step(loss)
            else:
                self.lr_scheduler.step()

        if self.rank == 0:
            # Epoch log
            log = self.train_metrics.result()
            log.update({'lr': lr})

            # Run validation at end of epoch
            if self.do_validation:
                self._validation_epoch(epoch, log)

            # Track with experiment tracker
            self.tracker.track(log, epoch=epoch, context={'subset': 'train'})

            return log

    def _validation_epoch(self, epoch, log):
        """
        :return: A log that contains information about validation
        """
        self.model.eval()

        if self.rank == 0:
            self.valid_metrics.reset()

        # Validation loop without gradient calculation/memory
        with torch.no_grad():
            for batch_idx, (data, target, file_indices) in enumerate(self.valid_dataloader):
                data, target = data.to(self.device), target.to(self.device)
                output = self.model(data)
                loss = self.criterion(output, target)

                if self.rank == 0:
                    self.valid_metrics.update('loss', loss.item())
                    for met in self.metric_ftns:
                        self.valid_metrics.update(met.__name__, met(output, target))

        if self.tracker.mode == "aim":
            self.tracker.track(self.valid_metrics.result(), epoch=epoch, context={'subset': 'validation'})
        else:
            log.update(**{'val_' + k: v for k, v in self.valid_metrics.result().items()})

    def _progress(self, batch_idx):
        base = '[{}/{} ({:.0f}%)]'
        if hasattr(self.dataloader, 'n_samples'):
            total = self.dataloader.n_samples
            current = min(total, batch_idx * self.dataloader.batch_size * self.world_size)
        else:
            current = batch_idx
            total = self.len_epoch
        return base.format(current, total, 100.0 * current / total)
