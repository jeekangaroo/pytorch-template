from dataloader.hdf5 import h5gen, h5test
import os

# -- PARAMETERS --
FILE_NAME = "../../mnist.h5"
GEN = True

# For hdf generation
if GEN:
    if os.path.exists(FILE_NAME):
        ans = input(f"{FILE_NAME} already exists, would you like to overwrite? (y/N): ") or "n"
        if ans.lower() == "y":
            os.remove(FILE_NAME)
            h5gen(FILE_NAME)
        else:
            print("Aborted")
    else:
        h5gen(FILE_NAME)

# For testing the generated hdf file
else:
    if not os.path.exists(FILE_NAME):
        print(f"{FILE_NAME} does not exist, can't read!")
    else:
        h5test(FILE_NAME, num_img=10, visualize=True)
