import os

import h5py
import numpy as np
from tqdm import tqdm

import dataloader.datasets


# Available datasets
dataset_configs = {
    "mnist.h5": ['MNIST', ['data/MNIST', True, None, None]],
}


def h5gen(file_name, chunk_size=10):
    assert os.path.basename(file_name) in dataset_configs.keys(),\
        "File name should be one of:" + str(dataset_configs.keys())

    # Get dataset class and instantiate the respective dataset object
    dataset_class = getattr(dataloader.datasets, dataset_configs[os.path.basename(file_name)][0])
    ds = dataset_class(*dataset_configs[os.path.basename(file_name)][1])
    print("Finished Loading the Dataset!")

    # Open an HDF5 file for writing
    file = h5py.File(file_name, "w")

    # Create dataset
    for i in tqdm(range(chunk_size), ascii=True, desc="Chunks"):
        imgs, labels, idxs = [], [], []
        for j in tqdm(range(i*len(ds)//chunk_size, (i+1)*len(ds)//chunk_size), ascii=True, desc="Images"):
            if ds[j] is None:
                continue

            # Get indexed item from the dataset
            img, label, idx = ds[j]
            img = np.array(img)

            # Collect into chunked lists
            imgs.append(img)
            labels.append(label)
            idxs.append(idx)

        # Convert into single array
        imgs = np.stack(imgs)
        labels = np.array(labels, dtype='int32')
        idxs = np.array(idxs, dtype='int32')

        # Create dataset on first chunk
        if i == 0:
            file.create_dataset("image", np.shape(imgs), maxshape=(None, *np.shape(imgs)[1:]), dtype=h5py.h5t.STD_U8BE, data=imgs)
            file.create_dataset("label", np.shape(labels), maxshape=(None,), dtype='int32', data=labels)
            file.create_dataset("idx", np.shape(labels), maxshape=(None,), dtype='int32', data=idxs)
        else:
            # Append to created dataset from the next chunk
            image_set = file["image"]
            image_set.resize(image_set.shape[0] + imgs.shape[0], axis=0)
            image_set[-imgs.shape[0]:] = imgs

            label_set = file["label"]
            label_set.resize(label_set.shape[0] + len(labels), axis=0)
            label_set[-len(labels):] = labels

            idx_set = file["idx"]
            idx_set.resize(idx_set.shape[0] + len(idxs), axis=0)
            idx_set[-len(idxs):] = idxs

    # close fully written HDF5 file
    file.close()
    return 0
