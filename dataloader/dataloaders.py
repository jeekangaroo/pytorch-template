import numpy as np
from torch.utils.data.dataset import Subset
from torch.utils.data.distributed import DistributedSampler
from torchvision import transforms

from base import BaseDataLoader
from dataloader.datasets import MNIST, MNIST_H5


class MnistDataLoader(BaseDataLoader):
    """
    MNIST data loading demo using BaseDataLoader
    """
    def __init__(self, data_dir, batch_size, shuffle=True, validation_split=0.0, h5=False,
                 distributed=False, rank=0, world_size=1, num_workers=1, training=True, prefetch_factor=2):

        self.validation_split = validation_split
        self.rank = rank
        self.world_size = world_size

        # Image transformation using torchvision
        composed_transforms = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])

        # Create dataset instance
        if h5:
            dataset = MNIST_H5(data_dir, transform=composed_transforms)
        else:
            dataset = MNIST(data_dir, train=training, transform=composed_transforms)

        # If distributed training, separate train and validation set if validation split given
        # and use Distributed Sampler to manage the data sampling.
        if distributed:
            dataset, sampler, shuffle = self.prep_distributed(dataset)
        else:
            sampler = None

        # Initialize the BaseDataLoader class with the set configuration
        super().__init__(dataset, batch_size, shuffle, validation_split, num_workers,
                         distributed=distributed, sampler=sampler, prefetch_factor=prefetch_factor)

    def prep_distributed(self, data_set):
        idx_full = np.arange(len(data_set))

        np.random.seed(0)
        np.random.shuffle(idx_full)

        len_valid = int(len(data_set) * self.validation_split)

        valid_idx = idx_full[0:len_valid]
        train_idx = np.delete(idx_full, np.arange(0, len_valid))

        train_dataset = Subset(data_set, train_idx)
        valid_dataset = Subset(data_set, valid_idx)

        sampler = DistributedSampler(train_dataset, rank=self.rank, num_replicas=self.world_size, shuffle=True)
        shuffle = False

        data_set = (train_dataset, valid_dataset)
        return data_set, sampler, shuffle
