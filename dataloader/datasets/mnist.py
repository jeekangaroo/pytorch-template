import os
from typing import Any, Callable, Dict, Optional, Tuple

from PIL import Image

import torch
from torch.utils.data.dataset import Dataset


# Sample MNIST Dataset
class MNIST(Dataset):
    training_file = 'training.pt'
    test_file = 'test.pt'
    classes = ['0 - zero', '1 - one', '2 - two', '3 - three', '4 - four',
               '5 - five', '6 - six', '7 - seven', '8 - eight', '9 - nine']

    @property
    def class_to_idx(self) -> Dict[str, int]:
        return {_class: i for i, _class in enumerate(self.classes)}

    def __init__(self, root: str, train: bool = True,
                 transform: Optional[Callable] = None, target_transform: Optional[Callable] = None) -> None:
        super().__init__()

        self.train = train  # training set or test set

        if self.train:
            data_file = self.training_file
        else:
            data_file = self.test_file

        self.transform = transform
        self.target_transform = target_transform

        self.data, self.targets = torch.load(os.path.join(root, data_file))

    def __len__(self) -> int:
        return 5000
        # return len(self.data)

    def __getitem__(self, index: int) -> Tuple[Any, Any, Any]:
        img, target = self.data[index], int(self.targets[index])
        img = Image.fromarray(img.numpy(), mode='L')

        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target, index
